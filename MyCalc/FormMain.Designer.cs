﻿namespace MyCalc
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonKiryJmi = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelScore = new System.Windows.Forms.Label();
            this.labelViewPointsForClick = new System.Windows.Forms.Label();
            this.textBoxViewUpgradeCost = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonBuyUpgrade = new System.Windows.Forms.Button();
            this.buttonBuyAutoClick = new System.Windows.Forms.Button();
            this.timerAutoClick = new System.Windows.Forms.Timer(this.components);
            this.labelViewPointsAuto = new System.Windows.Forms.Label();
            this.buttonExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonKiryJmi
            // 
            this.buttonKiryJmi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.buttonKiryJmi.ForeColor = System.Drawing.Color.White;
            this.buttonKiryJmi.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonKiryJmi.Location = new System.Drawing.Point(419, 423);
            this.buttonKiryJmi.Margin = new System.Windows.Forms.Padding(5);
            this.buttonKiryJmi.Name = "buttonKiryJmi";
            this.buttonKiryJmi.Size = new System.Drawing.Size(189, 35);
            this.buttonKiryJmi.TabIndex = 0;
            this.buttonKiryJmi.Text = "Киря, жми";
            this.buttonKiryJmi.UseVisualStyleBackColor = false;
            this.buttonKiryJmi.Click += new System.EventHandler(this.buttonKiryJmi_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(464, 274);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "ОЧКИ:";
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.BackColor = System.Drawing.Color.Transparent;
            this.labelScore.Font = new System.Drawing.Font("Myanmar Text", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScore.Location = new System.Drawing.Point(560, 265);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(33, 43);
            this.labelScore.TabIndex = 2;
            this.labelScore.Text = "0";
            // 
            // labelViewPointsForClick
            // 
            this.labelViewPointsForClick.AutoSize = true;
            this.labelViewPointsForClick.BackColor = System.Drawing.Color.Transparent;
            this.labelViewPointsForClick.Location = new System.Drawing.Point(241, 448);
            this.labelViewPointsForClick.Name = "labelViewPointsForClick";
            this.labelViewPointsForClick.Size = new System.Drawing.Size(151, 20);
            this.labelViewPointsForClick.TabIndex = 3;
            this.labelViewPointsForClick.Text = "Очков за клик: 1";
            // 
            // textBoxViewUpgradeCost
            // 
            this.textBoxViewUpgradeCost.Location = new System.Drawing.Point(12, 400);
            this.textBoxViewUpgradeCost.Name = "textBoxViewUpgradeCost";
            this.textBoxViewUpgradeCost.ReadOnly = true;
            this.textBoxViewUpgradeCost.Size = new System.Drawing.Size(174, 32);
            this.textBoxViewUpgradeCost.TabIndex = 4;
            this.textBoxViewUpgradeCost.Text = "50";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(11, 377);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Стоимость апгрейда";
            // 
            // buttonBuyUpgrade
            // 
            this.buttonBuyUpgrade.Enabled = false;
            this.buttonBuyUpgrade.Location = new System.Drawing.Point(12, 438);
            this.buttonBuyUpgrade.Name = "buttonBuyUpgrade";
            this.buttonBuyUpgrade.Size = new System.Drawing.Size(174, 30);
            this.buttonBuyUpgrade.TabIndex = 6;
            this.buttonBuyUpgrade.Text = "Купить Апгрейд";
            this.buttonBuyUpgrade.UseVisualStyleBackColor = true;
            this.buttonBuyUpgrade.Click += new System.EventHandler(this.buttonBuyUpgrade_Click);
            // 
            // buttonBuyAutoClick
            // 
            this.buttonBuyAutoClick.Enabled = false;
            this.buttonBuyAutoClick.Location = new System.Drawing.Point(12, 475);
            this.buttonBuyAutoClick.Name = "buttonBuyAutoClick";
            this.buttonBuyAutoClick.Size = new System.Drawing.Size(223, 38);
            this.buttonBuyAutoClick.TabIndex = 7;
            this.buttonBuyAutoClick.Text = "Купить автоклик за 100";
            this.buttonBuyAutoClick.UseVisualStyleBackColor = true;
            this.buttonBuyAutoClick.Click += new System.EventHandler(this.buttonBuyAutoClick_Click);
            // 
            // timerAutoClick
            // 
            this.timerAutoClick.Enabled = true;
            this.timerAutoClick.Interval = 1000;
            this.timerAutoClick.Tick += new System.EventHandler(this.timerAutoClick_Tick);
            // 
            // labelViewPointsAuto
            // 
            this.labelViewPointsAuto.AutoSize = true;
            this.labelViewPointsAuto.BackColor = System.Drawing.Color.Transparent;
            this.labelViewPointsAuto.ForeColor = System.Drawing.Color.Black;
            this.labelViewPointsAuto.Location = new System.Drawing.Point(241, 484);
            this.labelViewPointsAuto.Name = "labelViewPointsAuto";
            this.labelViewPointsAuto.Size = new System.Drawing.Size(192, 20);
            this.labelViewPointsAuto.TabIndex = 8;
            this.labelViewPointsAuto.Text = "Очков за автоклик: 0";
            this.labelViewPointsAuto.Click += new System.EventHandler(this.labelViewPointsAuto_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(510, 541);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(98, 28);
            this.buttonExit.TabIndex = 9;
            this.buttonExit.Text = "Выход";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::MyCalc.Properties.Resources.hqdefault;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(634, 595);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.labelViewPointsAuto);
            this.Controls.Add(this.buttonBuyAutoClick);
            this.Controls.Add(this.buttonBuyUpgrade);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxViewUpgradeCost);
            this.Controls.Add(this.labelViewPointsForClick);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonKiryJmi);
            this.Font = new System.Drawing.Font("MV Boli", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Кликер";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonKiryJmi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Label labelViewPointsForClick;
        private System.Windows.Forms.TextBox textBoxViewUpgradeCost;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonBuyUpgrade;
        private System.Windows.Forms.Button buttonBuyAutoClick;
        private System.Windows.Forms.Timer timerAutoClick;
        private System.Windows.Forms.Label labelViewPointsAuto;
        private System.Windows.Forms.Button buttonExit;
    }
}

