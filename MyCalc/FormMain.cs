﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyCalc
{
    public partial class FormMain : Form
    {
        private int score,
                    pointsForClick,
                    upgradeCost,
                    autoClickCost,
                    pointsAuto;

        public FormMain()
        {
            InitializeComponent();
        }

        private void timerAutoClick_Tick(object sender, EventArgs e)
        {
            score += pointsAuto;
            labelScore.Text = score.ToString();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void labelViewPointsAuto_Click(object sender, EventArgs e)
        {

        }

        private void buttonBuyAutoClick_Click(object sender, EventArgs e)
        {
            score -= autoClickCost;
            labelScore.Text = score.ToString();

            pointsAuto++;
            labelViewPointsAuto.Text = "Очков за автоклик: " + pointsAuto.ToString();

            autoClickCost *= 3;

            buttonBuyAutoClick.Text = "Купить автоклик за: " + autoClickCost.ToString();

            if (score < autoClickCost)
            {
                buttonBuyAutoClick.Enabled = false;
            }

            if (score < upgradeCost)
            {
                buttonBuyUpgrade.Enabled = false;
            }
        }

        private void buttonBuyUpgrade_Click(object sender, EventArgs e)
        {
            score -= upgradeCost;
            labelScore.Text = score.ToString();

            pointsForClick++;
            labelViewPointsForClick.Text = "Очков за клик: " + pointsForClick.ToString();

            upgradeCost *= 2;
            textBoxViewUpgradeCost.Text = upgradeCost.ToString();

            if (score < upgradeCost)
            {
                buttonBuyUpgrade.Enabled = false;
            }

            if (score < autoClickCost)
            {
                buttonBuyAutoClick.Enabled = false;
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            score = 0;
            pointsForClick = 1;
            upgradeCost = 50;
            autoClickCost = 100;
            pointsAuto = 0;
        }

        private void buttonKiryJmi_Click(object sender, EventArgs e)
        {
            score += pointsForClick;
            labelScore.Text = score.ToString();
            if (score >= upgradeCost)
            {
                buttonBuyUpgrade.Enabled = true;
            }
            if (score >= autoClickCost)
            {
                buttonBuyAutoClick.Enabled = true;
            }
        }
    }
}
